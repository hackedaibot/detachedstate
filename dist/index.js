"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const define_property_1 = __importDefault(require("define-property"));
function createStateAccessor(factory) {
    var map = new WeakMap();
    define_property_1.default(accessor, "dispose", () => map = null);
    return accessor;
    function accessor(object) {
        if (!map.has(object)) {
            const state = factory(object);
            map.set(object, state);
        }
        return map.get(object);
    }
}
exports.createStateAccessor = createStateAccessor;
const L = createStateAccessor;
function createBinaryRelationAccessor(factory) {
    const getter = L(A => L(B => factory(A, B)));
    return (o1, o2) => getter(o1)(o2);
}
exports.createBinaryRelationAccessor = createBinaryRelationAccessor;
function createSymmetryBinaryRelationAccessor(factory) {
    const enumer = createEnumerator();
    const getter = createBinaryRelationAccessor(factory);
    return (o1, o2) => getter(...enumer([o1, o2]));
}
exports.createSymmetryBinaryRelationAccessor = createSymmetryBinaryRelationAccessor;
function createEnumerator() {
    var i = 1, m = new WeakMap();
    const sortFn = (a, b) => m.get(a) - m.get(b);
    function enshureOrder(o) {
        if (!m.has(o))
            m.set(o, i++);
    }
    return function sort(objects) {
        objects.forEach(enshureOrder);
        return objects.sort(sortFn);
    };
}
exports.createEnumerator = createEnumerator;
//# sourceMappingURL=index.js.map