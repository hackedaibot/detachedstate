import { Factory, Accessor } from "./types";
export declare function createStateAccessor<U extends object, S = any>(factory: Factory<U, S>): Accessor<U, S>;
export declare function createBinaryRelationAccessor<U extends object, S = any>(factory: Function): (o1: U, o2: U) => any;
export declare function createSymmetryBinaryRelationAccessor<U extends object, S = any>(factory: Function): (o1: U, o2: U) => any;
export declare function createEnumerator(): (objects: object[]) => object[];
