export declare type Factory<U, S> = (...object: U[]) => S;
export interface Accessor<U, S> {
    (object: U): S;
    dispose(): boolean;
}
export interface MappingController {
    del(): boolean;
    has(): boolean;
    get(): any;
    set(s: any): any;
}
