# DetachedState

A very simple to use utility for handing associated meta-state of objects. Relies on WeakMap and doesn&#39;t prevent garbage collection of inaccessible data.

## Simplest usage 

```js
const createStateResolver = require("../dist").default


const factory = subject => {

	const state = { subject, debt: 0 };

	state.borrow = (amount) => {
		state.debt    +=  amount;
		subject.amount += amount;
	}
	state.repay  = (amount) => {
		state.debt     += -amount
		subject.amount += -1.1 * amount; // 10% fee
	};
	state.print  = () => console.log(`BANK:: You, ${subject.name}, own me ${state.debt}. ${
		state.debt > 0 ? "When will I have my money???" : "Well done."
	}`)

	return state
}

const creditor = createStateResolver(factory)
```

Then the newly created `creditor` can be used to get separate states for each object.
Lets check out a story why it's better not to borrow money from banks.


```js
const man1 = { name: "Person 1", amount: 1000 }
const man2 = { name: "Person 2", amount: 0 }

console.log("Here is man1 and man2:")
console.log(man1)
console.log(man2)


/**
 * @output
 * Here is man1 and man2:
 * { name: 'Person 1', amount: 1000 }
 * { name: 'Person 2', amount: 0 }
 */


console.log("Person 2 borrows 100$:")
creditor(man2).borrow(100);

/**
 * @output
 * Person 2 borrows 100$:
 */


console.log("Person 2 now have 100$");
console.log(man2)

/**
 * @output
 * Person 2 now have 100$
 * { name: 'Person 2', amount: 100 }
 */



console.log("Person 2 though is in trouble");
creditor(man2).print();

/**
 * @output
 * Person 2 though is in trouble
 * BANK:: You, Person 2, own me 100. When will I have my money???
 */


console.log("Person 1 has no problems. He didnt borrowed money.");
creditor(man1).print();
console.log(man1)


/**
 * @output
 * Person 1 has no problems. He didnt borrowed money.
 * BANK:: You, Person 1, own me 0. Well done.
 * { name: 'Person 1', amount: 1000 }
 * Person 2 returns some money but there is 10% fee...
 * BANK:: You, Person 2, own me 10. When will I have my money???
 */


console.log("Person 2 returns some money but there is 10% fee...");
creditor(man2).repay(90);
creditor(man2).print();

/**
 * @output
 * Person 2 returns some money but there is 10% fee...
 * BANK:: You, Person 2, own me 10. When will I have my money???
 */




```


## Never send MappingController instance to the outer scopes

The returned object contains an implicit reference to an original object in methods' closures. 
If you use it outside of factory scope, memory leakage is likely to be introduced. This is not considered a bug.

You can actually have MappingController instance out of factory scope in some cases, but it becomes your responsibility to prevent memory leakage by disposing controller.

## Testing

To run tests, run `yarn run test`:

```text
$ vows tests/* --spec

  ♢ Detached State Accessor
  
  when creating detached state accessor, the result
    ✓ ...is a function
    ✓ ...has a .dispose method
    ✓ ...is expecting 1 argument
    ✓ ...throws on non-objects: numbers
    ✓ ...throws on non-objects: null
    ✓ ...throws on non-objects: string
    ✓ ...throws if no arguments
    ✓ ...doesn't throw on functions
    ✓ ...return mutable state object as provided by factory
    ✓ ...passes object to factory function
    ✓ ...return same mutable state object on second req
    ✓ ...doesn't throw on objects
    ✓ ...if two objects have same reference, then accessor will yield the same states
    ✓ ...if two objects have only same value, they will have different states
    ✓ ...spread operator will result in different object being created, thus different state
    ✓ ...factory function provide (common) object to all states by reference
    ✓ ...factory function provide new (state) object to each state

✓ OK » 17 honored (0.007s)
  
Done in 0.15s.
```