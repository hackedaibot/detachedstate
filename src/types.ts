
export type Factory<U, S> = (...object:U[]) => S;

export interface Accessor<U, S> {
    (object: U): S;
    dispose(): boolean;
}

 
export interface MappingController {
    del (): boolean;    /** remove object's state */
    has (): boolean;    /** returns if object state has been built */
    get (): any;        /** returns object state if it has been built or null */
    set (s: any): any;  /** set arbitrary object state  */
}
