import { Factory, Accessor } from "./types";
import define from 'define-property';
import { func } from "prop-types";

/**	
 * Allows user to construct a function of a single object argument,
 * that returns associated its metastate. 
 * 
 * @module 	detached_state
 * @param   {Factory}  factory function returning a new state object for a specified object
 * @param   {Object}   common  object that can be used by factory to create shared state
 * @returns {Accessor}         function resolving associated state or creating a new one.
 */

export function createStateAccessor<U extends object, S=any>(factory: Factory<U, S>){
    var map = new WeakMap<U, S>() as any;

    define(accessor, "dispose", () => map = null);
    return accessor as Accessor<U, S>;

    function accessor(object: U): S {
        if (!map.has(object)) {
            const state = factory(object)
            map.set(object, state)
        }
        return map.get(object) as S;
    }
}

// Shorthand
const L = createStateAccessor;


export function createBinaryRelationAccessor<U extends object, S=any>(factory: Function){
    const getter = L(A => L(B => factory(A, B)));
    return (o1: U, o2: U) => getter(o1)(o2);
}
 

export function createSymmetryBinaryRelationAccessor<U extends object, S=any>(factory: Function){
    const enumer = createEnumerator()
    const getter = createBinaryRelationAccessor(factory);
    // @ts-ignore
    return (o1: U, o2: U) => getter(...enumer([ o1, o2 ]))
}


export function createEnumerator() {
    var i = 1, m = new WeakMap();
    const sortFn = (a, b) => m.get(a) - m.get(b)

    function ensureOrder(o) {
        if (!m.has(o)) m.set(o, i++);
    }

    return function sort(objects: object[]) {
        objects.forEach(ensureOrder);
        return objects.sort(sortFn)
    }
}