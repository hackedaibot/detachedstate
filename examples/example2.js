
const createResolver = require("../dist").default
const log = console.log.bind(console)

const f = () => ({ amount: 0 })
// Define each currency meta-layer with data about corresponding amounts allocations 
const BTC = createResolver(f), ETH = createResolver(f);

// Define two objects representing users
const u1 = { name: "Vasia" } // !!! We do not have any other requirements 
const u2 = { name: "Petia" } // !!! Other that they must be objects
							 
// Define helpers
function setBalances(user, btc, eth) {
	BTC(user).amount = btc; 
	ETH(user).amount = eth;
}
function MoveMoney(W, FM, TO, amount) {
	amount = amount || W(FM).amount;
	W(FM).amount += -amount; 
	W(TO).amount +=  amount;
}

function LogData(U = [u1, u2], W = [ BTC, ETH ]) { 
	for (let u of U) log(u.name, ...W.map(w => w(u).amount))
}

// Fine! Now lets run GOP STOP Scenario
setBalances(u1, 1000, 900) // Define balances at start
setBalances(u2, 10, 8)
LogData(); // ('Vasia' 1000, 900),   ('Petia' 10, 8)

log("u1: Гоп стоп");
MoveMoney(BTC, u2, u1); // Steal all BTC
log("u2: Hу хоть эфиры на месте =(")
LogData();  // ('Vasia' 1010, 900),   ('Petia' 0, 8)

log("u1: Лол"); 
MoveMoney(ETH, u2, u1); // Steal all ETH
log("u2: =(")
LogData(); // ('Vasia' 1010, 908),   ('Petia' 0, 0)
// Petia is very sad cauze of this shit

orderSlutsByBTC(u1, BTC(u1).amount - 100)
log("u1: EEEEEE..."); 
log("u2: FUUUUU..."); 

LogData();      // ('Vasia' 110, 908),   ('Petia' 0, 0)
log(u1) // { name: 'Vasia', spentOnSluts: 910 }

function orderSlutsByBTC(user, amount) {
	user.spentOnSluts = user.spentOnSluts || 0;
	user.spentOnSluts += amount;
	BTC(user).amount += -amount;
}