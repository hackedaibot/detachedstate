const createStateResolver = require("../dist").default


const creditor = createStateResolver(subject => {

	const state = { subject, debt: 0 };

	state.borrow = (amount) => {
		state.debt    +=  amount;
		subject.amount += amount;
	}
	state.repay  = (amount) => {
		state.debt     += -amount
		subject.amount += -1.1 * amount; // 10% fee
	};
	state.print  = () => console.log(`BANK:: You, ${subject.name}, own me ${state.debt}. ${
		state.debt > 0 ? "When will I have my money???" : "Well done."
	}`)

	return state
})


///////// DO NOT USE EVAL LIKE THAT IN PRODUCTION.
///////// THIS IS GREAT FOR DEMO ONLY!!!!!!!!

let i = 0;

function execute(code, ...rest) {
	if (!code) return
	i++

	console.group(" (" + i + ") ACT")

	console.group("code:")
	console.log(code)
	console.groupEnd()
	console.log(".")

	console.group("output:")
	eval(code)
	console.groupEnd()
	console.groupEnd()


	console.log("\n")

	setImmediate(execute, ...rest);
}

const man1 = { name: "Person 1", amount: 1000 }
const man2 = { name: "Person 2", amount: 0 }

execute(`console.log("Here is man1 and man2:")
console.log(man1)
console.log(man2)`, 
`console.log("Person 2 borrows 100$:")
creditor(man2).borrow(100);`, 
`console.log("Person 2 now have 100$");
console.log(man2)`, 
`console.log("Person 2 though is in trouble");
creditor(man2).print();`, 
`console.log("Person 1 has no problems. He didnt borrowed money.");
creditor(man1).print();
console.log(man1)`, 
`console.log("Person 2 returns some money but there is 10% fee...");
creditor(man2).repay(90);
creditor(man2).print();`)