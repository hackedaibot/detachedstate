const createStateResolver = require("../dist").default


const factory = subject => {

	const state = { subject, debt: 0 };

	state.borrow = (amount) => {
		state.debt    +=  amount;
		subject.amount += amount;
	}
	state.repay  = (amount) => {
		state.debt     += -amount
		subject.amount += -1.1 * amount; // 10% fee
	};
	state.print  = () => console.log(`BANK:: You, ${subject.name}, own me ${state.debt}. ${
		state.debt > 0 ? "When will I have my money???" : "Well done."
	}`)

	return state
}

const creditor = createStateResolver(factory)


const man1 = { name: "Person 1", amount: 1000 }
const man2 = { name: "Person 2", amount: 0 }

console.log("Here is man1 and man2:")
console.log(man1)
console.log(man2)


/**
 * @output
 * Here is man1 and man2:
 * { name: 'Person 1', amount: 1000 }
 * { name: 'Person 2', amount: 0 }
 */


console.log("Person 2 borrows 100$:")
creditor(man2).borrow(100);

/**
 * @output
 * Person 2 borrows 100$:
 */


console.log("Person 2 now have 100$");
console.log(man2)

/**
 * @output
 * Person 2 now have 100$
 * { name: 'Person 2', amount: 100 }
 */



console.log("Person 2 though is in trouble");
creditor(man2).print();

/**
 * @output
 * Person 2 though is in trouble
 * BANK:: You, Person 2, own me 100. When will I have my money???
 */


console.log("Person 1 has no problems. He didnt borrowed money.");
creditor(man1).print();
console.log(man1)


/**
 * @output
 * Person 1 has no problems. He didnt borrowed money.
 * BANK:: You, Person 1, own me 0. Well done.
 * { name: 'Person 1', amount: 1000 }
 * Person 2 returns some money but there is 10% fee...
 * BANK:: You, Person 2, own me 10. When will I have my money???
 */


console.log("Person 2 returns some money but there is 10% fee...");
creditor(man2).repay(90);
creditor(man2).print();

/**
 * @output
 * Person 2 returns some money but there is 10% fee...
 * BANK:: You, Person 2, own me 10. When will I have my money???
 */


