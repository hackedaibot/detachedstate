const detached = require("../dist").createStateAccessor;
const detached2 = require("../dist").createBinaryRelationAccessor;
const detachedSym = require("../dist").createSymmetryBinaryRelationAccessor;
const vows     = require('vows');
const assert   = require('assert');

const common = { common: true }
const factory = object => ({ state: {}, num: 1, common, object })


const objA = {}, objB = {};

// Create a Test Suite
vows.describe('Detached State Accessor').addBatch({

    'when creating detached state accessor, the result': {

       	topic: () => detached(factory),

       	 '...is a function': function (topic) {
            	assert.typeOf(topic, "function");
		},
		
		'...has a .dispose method': function (topic) {
            	assert.typeOf(topic.dispose, "function");
		},

		'...is expecting 1 argument': function (topic) {
            	assert.equal(topic.length, 1);
		},

		'...throws on non-objects: numbers': function (topic) {
            	assert.throws(() => topic(1));
		},

		'...throws on non-objects: null': function (topic) {
            	assert.throws(() => topic(null));
		},

		'...throws on non-objects: string': function (topic) {
            	assert.throws(() => topic("string"));
		},
		
		'...throws if no arguments': function (topic) {
            	assert.throws(() => topic());
		},
		'...doesn\'t throw on functions': function (topic) {
            	assert.doesNotThrow(() => topic(factory));
            	assert.doesNotThrow(() => topic(topic));
		},

		"...return mutable state object as provided by factory": function (topic)  {
			topic(factory).state.check = true
		},

		"...passes object to factory function": function (topic)  {
			const obj1 = {};
			assert.equal(topic(obj1).object, obj1)
		},


		"...return same mutable state object on second req": function (topic)  {
			assert.equal(topic(factory).state.check, true)
		},

		
		'...doesn\'t throw on objects': function (topic) {
			const obj1 = {};
            	assert.doesNotThrow(() => topic(obj1));
		},
		'...if two objects have same reference, then accessor will yield the same states': function (topic) {
			const obj1 = {};
			const obj2 = obj1;
			const state1 = topic(obj1)
			const state2 = topic(obj2)
            	assert.equal(state1, state2);
            	assert.deepEqual(state1, state2);
		},

		'...if two objects have only same value, they will have different states': function (topic) {
			const obj1 = {}, obj2 = {};
			const state1 = topic(obj1)
			const state2 = topic(obj2)
         	  	assert.notEqual(state1, state2);
          		assert.deepEqual(state1, state2);
		},

		'...spread operator will result in different object being created, thus different state': function (topic) {
			const obj1 = {};
			const state1 = topic(obj1)
			const state2 = topic({ ...obj1 })
            	assert.notEqual(state1, state2);
            	assert.deepEqual(state1, state2);
		},

		'...factory function provide (common) object to all states by reference': function (topic) {
			const obj1 = {}, obj2 = {};
			const state1 = topic(obj1);
			const state2 = topic(obj2);
            	assert.equal(state1.common, state2.common);
		},

		'...factory function provide new (state) object to each state': function (topic) {
			const obj1 = {}, obj2 = {};
			const state1 = topic(obj1);
			const state2 = topic(obj2);
            	assert.notEqual(state1.state, state2.state);
		},


    },

    'when creating createBinaryRelationAccessor, the result': {
	  topic: () => detached2((a1, a2) => ({ objects: { a1, a2 } }) ),

	  '...if two pairs of objects have same reference, then accessor will yield the same states': function (topic) {
		const obj1 = objA;
		const obj2 = objB;
		const state1 = topic(obj1, obj2)
		const state2 = topic(objA, objB)
		assert.equal(state1, state2);
		assert.deepEqual(state1, state2);
	},

	'...order does matter and will return a different state object': function (topic) {
		const obj1 = objB;
		const obj2 = objA;
		const state1 = topic(obj1, obj2)
		const state2 = topic(objA, objB)
		assert.notEqual(state1, state2);
	},

    },

    'when creating createSymmetricBinaryRelationAccessor, the result': {
	topic: () => detachedSym((a1, a2) => ({ objects: { a1, a2 } }) ),

	'...if two pairs of objects have same reference, then accessor will yield the same states': function (topic) {
	    const obj1 = objA;
	    const obj2 = objB;
	    const state1 = topic(obj1, obj2)
	    const state2 = topic(objA, objB)
	    assert.equal(state1, state2);
	    assert.deepEqual(state1, state2);
    },

    '...order does NOT matter:': function (topic) {
	    const obj1 = objB;
	    const obj2 = objA;
	    const state1 = topic(obj1, obj2)
	    const state2 = topic(objA, objB)
	    assert.equal(state1, state2);
    },

  }

}).export(module); // Run it
